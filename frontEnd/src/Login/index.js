import { Link , useNavigate} from 'react-router-dom';
import { useState, useRef, useEffect} from 'react';
import Cookies from 'js-cookie'
import "./index.css";

const Login = ()=>{
    const [value, setValue] = useState(false);
    const [jsonData, setJsonData] = useState({});
    const [msg, setMsg] = useState("");
    const email = useRef(null);
    const password = useRef(null); 

    const navigate = useNavigate();

    const loginSuccess=(jsonObject)=>{
          console.log(jsonObject)
          const fifteenMinutes = new Date(new Date().getTime()+1000*60*15)
          Cookies.set("jwt_token", jsonObject.jwtToken, {expires:fifteenMinutes})
          navigate("/");
    }
    
    const onsubmitLoginForm=(e)=>{
        e.preventDefault();
        setValue(!value);
        setJsonData({
            "email":email.current.value,
            "password":password.current.value,
        })
        
        document.getElementById("email").value = "";
        document.getElementById("password").value = "";
    }

    useEffect(()=>{
         if(value){       
        async function fetchData(){
        if(Object.keys(jsonData).length !== 0){
        const response = await fetch("http://localhost:5000/api/login", {
            method:"POST",
            mode:"cors",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            body:JSON.stringify(jsonData),
        });
        console.log(response)
        if(response.ok === true){
            const jsonObject = await response.json();
            loginSuccess(jsonObject);
        }else{
            const jsonObject = await response.json()
            setMsg(jsonObject.msg)
        }
    }
        
       }
    
       fetchData();}
    }, [value]);


return (
    <div className='App'>
    <div className='login-card'>
    <h1 className='heading'>Welcome Back!</h1>
    <p className='para'>
            Sem nunc eliquet tellus bibundum iaqulis aliquam curabitur vitae porttitor, ligula
            mi voluptatum feugiat sapien lacus
    </p>

    <form className='form' onSubmit={onsubmitLoginForm}>
    <label htmlFor="email" className='label'> EMAIL </label>
    <input ref={email} id='email' type="text" placeholder='Enter your email' className='input' required />
    
    <label htmlFor='password' className='label'> PASSWORD </label>
    <input ref={password} id='password' type="password" placeholder='Enter your password' className='input'required />

    <div className='remember-forgotPassword-container'>
        <div className='remember-container'>
            <input id='remember' className='input-remember' type="checkbox"/>
            <label htmlFor='remember' className='remeber-para'>Remember Me</label>
        </div>

        <p className='forgot-para'>forgot password?</p>
    </div>

    <button className='login-button' type='submit'>LOGIN</button>
    </form>
    <p className='errorMsg'>{msg}</p>
    <Link className='link' to="/SignUp"><p className='sign-up-para'>SIGN UP</p></Link> 
</div>
</div>
)
}

export default Login

