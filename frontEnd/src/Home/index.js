import {useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import Cookies from 'js-cookie';
import './index.css'

const Home=()=>{
    const [logout, setLogout] = useState(false);

    const navigate = useNavigate();

    const onClickLogout=()=>{
        Cookies.remove('jwt_token');
        setLogout(!logout)
        navigate("/login")
    }

    useEffect(()=>{
        const jwt = Cookies.get("jwt_token");
        console.log(jwt);
        async function fetchApi(){
             await fetch("http://localhost:5000/api/logout", {
               method:"delete"
             });
        }
        fetchApi()

        if(jwt === undefined){
            navigate('/login');
        }
    },[logout])

    return(
    <div className='Home'>
             <img src='https://www.shutterstock.com/image-vector/house-icon-door-outline-design-260nw-742583137.jpg' alt="home" className='image'/>
             <button onClick={onClickLogout} className='logout-button'>Logout</button>
    </div>
)
}

export default Home