import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import './index.css';

const SignUp = () => {
    const [Msg, setMsg] = useState("");
    
    const navigate = useNavigate()

    const signUpSuccess=(value)=>{
         if(value){
             navigate("/login");
         }
    }

    const signUp = async(e)=>{
        e.preventDefault();
        let firstName = document.getElementById('firstName').value;
        let lastName = document.getElementById('lastName').value;
        let email = document.getElementById('email').value;
        let password = document.getElementById('password').value;
        
        const jsonData = {
            firstName,
            lastName,
            email,
            password
        }
        const response = await fetch("http://localhost:5000/api/register", {
            method:"POST",
            mode:"cors",
            headers:{
                "Content-Type":"application/json"
            },
            body:JSON.stringify(jsonData),
        });
        const responseData = await response.json();
        console.log(response);

        if(response.ok !== true){
            setMsg(responseData.msg);
        }else{
          signUpSuccess(response.ok)
        }

        document.getElementById('firstName').value=""
        document.getElementById('lastName').value=""
        document.getElementById('email').value=""
        document.getElementById('password').value=""

        }
        
    return (
    <div className='App'>
    <div className='login-card'>
        <h1 className='heading'>Create your Account!</h1>
        <p className='para'>
                Sem nunc eliquet tellus bibundum iaqulis aliquam curabitur vitae porttitor, ligula
                mi voluptatum feugiat sapien lacus
        </p>

        <form className='form' onSubmit={signUp}>
         <div className='firstName-lastName-container'>
                <div className='name-container'>
                <label htmlFor="firstName" className='label'> FIRST NAME </label>
                <input id='firstName' type="text" placeholder='Enter your first name' className='input name-first-input' required/>
                </div>
                <div className='name-container'>
                <label htmlFor="lastName" className='label'> LAST NAME </label>
                <input id='lastName' type="text" placeholder='Enter your last name' className='input name-last-input' required/>
                </div>
         </div>

        <label htmlFor="email" className='label'> EMAIL </label>
        <input id='email' type="text" placeholder='Enter your email' className='input' required/>
        
        <label htmlFor='password' className='label'> PASSWORD </label>
        <input id='password' type="password" placeholder='Enter your password' className='input' required/>

        <button className='login-button' type='submit'>SIGN UP</button>
        </form>
         <p className='errorMsg'>{Msg}</p>
         <Link className='link' to="/login"><p className='login-para'>LOGIN</p></Link>
    </div>
    </div>
    )
    }

export default SignUp;