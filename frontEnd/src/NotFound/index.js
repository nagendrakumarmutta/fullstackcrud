import { useNavigate } from 'react-router'
import './index.css'

const NotFound = ()=>{
    const navigate = useNavigate()
    const onclickBtn=()=>navigate("/")
    
    return(
        <div className='notFound'>
            <h1 className='not-found-heading'>Not Found</h1>
            <button onClick={onclickBtn} className='not-found-button'>Go to Home</button>
        </div>
    )
}

export default NotFound