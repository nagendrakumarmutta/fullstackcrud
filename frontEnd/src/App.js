import Login from './Login'
import SignUp from './SignUp';
import Home from './Home';
import NotFound from './NotFound';
import {BrowserRouter as Router,Routes, Route} from 'react-router-dom';
import './App.css';

function App() {
  return (
        <Router>
           <Routes>
              <Route  path="/SignUp"    element = {<SignUp />}/>
              <Route  path="/login"     element = {<Login />}/> 
              <Route  exact path="/"    element = {<Home />}/> 
              <Route  path='*'          element = {<NotFound />}/>
            </Routes>
        </Router>
  );
}

export default App;
