const express = require("express");
const cors = require('cors');
const sequelize = require("./dbConnection");
const models = require("./models");
const router = require("./routes/apis");



const app = express();
app.use(express.json());
app.use(cors());
app.use("/api", router);


const initializeDbConnection=()=>{
    try{
    app.listen(5000,()=>{
        console.log(`server is running at localhost:5000`)
    })}
    catch(err){
        console.log(error)
    }
}
 

initializeDbConnection();  